#!/bin/bash
mkdir public
cd docs/simple-animation-js/ || exit
DIR=$(ls -td -- * | head -n 1)
cd "$DIR" || exit
cp -r ./* ../../../public
